<?php

namespace Vitoop\InfomgmtBundle\Utils\Token;

interface TokenGeneratorInterface
{
    public function generateToken();
}
