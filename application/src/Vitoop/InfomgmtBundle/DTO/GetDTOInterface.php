<?php

namespace Vitoop\InfomgmtBundle\DTO;

interface GetDTOInterface
{
    public function getDTO();
}
