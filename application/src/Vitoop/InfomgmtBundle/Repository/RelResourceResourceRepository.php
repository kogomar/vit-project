<?php

namespace Vitoop\InfomgmtBundle\Repository;

use Doctrine\ORM\EntityRepository;

use Vitoop\InfomgmtBundle\Entity\Project;
use Vitoop\InfomgmtBundle\Entity\RelResourceResource;
use Vitoop\InfomgmtBundle\Entity\Resource;
use Vitoop\InfomgmtBundle\Entity\User;

/**
 * RelResourceResourceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RelResourceResourceRepository extends EntityRepository
{
    /**
     * @param RelResourceResource $relation
     */
    public function add(RelResourceResource $relation)
    {
        $this->_em->persist($relation);
    }

    public function getOneFirstRel(Resource $resource1, Resource $resource2)
    {
        return $this->createQueryBuilder('r')
            ->select('r')
            ->where('r.resource1 = :resource1')
            ->andWhere('r.resource2 = :resource2')
            ->andWhere('r.deletedByUser IS NULL')
            ->setParameter('resource1', $resource1)
            ->setParameter('resource2', $resource2)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getCountOfAddedResources($userID, $resourceID)
    {
        return $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->where('r.resource2 = :resourceID')
            ->andWhere('r.user = :userID')
            ->setParameter('resourceID', $resourceID)
            ->setParameter('userID', $userID)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getCountOfRemovedResources($userID, $resourceID)
    {
        return $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->where('r.resource2 = :resourceID')
            ->andWhere('r.deletedByUser = :userID')
            ->setParameter('resourceID', $resourceID)
            ->setParameter('userID', $userID)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getCoefficients(Project $project)
    {
        return $this->createQueryBuilder('rrr')
            ->select('res.id as res_id')
            ->addSelect('rrr.id as rel_id')
            ->addSelect('rrr.coefficient as coefficient')
            ->innerJoin('rrr.resource2', 'res')
            ->where('rrr.resource1 =:project')
            ->setParameter('project', $project)
            ->getQuery()
            ->getResult();
    }


    public function exists(RelResourceResource $rrr)
    {
        return $this->getEntityManager()
                    ->createQuery('SELECT rrr.id FROM Vitoop\InfomgmtBundle\Entity\RelResourceResource rrr
            WHERE rrr.resource1=:arg_resource1 AND rrr.resource2=:arg_resource2 AND rrr.user=:arg_user')
                    ->setParameters(array(
                'arg_resource1' => $rrr->getResource1(),
                'arg_resource2' => $rrr->getResource2(),
                'arg_user' => $rrr->getUser()
            ))
            ->getResult();
    }
}