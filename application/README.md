Vitoop Project

Commands:

 Start prod enviroument

```sh

make start 

```

 Install all dependencies and get last packages and updates

```sh

make install 

```

 Load dump from mysql in project

```sh

make load_db path=<path_to_mysqldump.sql> 

```
 
 Stop prod enviroument
 
 ```sh
 
 make stop 
 
 ```